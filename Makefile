
SRC1 = hello.c
SRC2 = bye.c
OUT1 = libhello.so
OUT2 = libbye.so
RESULT = run
MAIN = main.c
EXECUTABLE =gcc -L./ -Wl,-rpath=./ -Wall -o $(RESULT) $(MAIN) -lbye -lhello
main: libs 
	$(EXECUTABLE)
result:
	$(EXECUTABLE)
libs:
	gcc -shared -o $(OUT1) -Wall -fPIC $(SRC1)
	gcc -shared -o $(OUT2) -Wall -fPIC $(SRC2)
clean:
	rm *.so
	rm run

